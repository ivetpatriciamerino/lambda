exports.handler = async (event) => {
    //Función para la conversión de unidades de fuerza
    /*
        Dina
        new  khuuyiuyiyton
        Fuerza
        kilogramo fuerza
        kilopondio
        gramo-fuerza
        libra-fuerza
        poundal
        kilonewton
        meganewton
    */
    //unidad del dato a convertir
    //esta es la funcion lambda automatizada
    let unidadEntrada = event.unidadDato;
    //dato a convertir
    let datoConvertir = Number(event.dato);
    //unidad a convertir
    let unidadSalida = event.unidadFinal;
    //dato convertido
    let datoConvertido = 0;

    if(unidadEntrada == unidadSalida)
    {
        
    }
    else
    {
        if(unidadEntrada == "newtons")
        {
            if(unidadSalida == "dina")
            {
                datoConvertido = datoConvertir * 100000;
            }
            if(unidadSalida == "libra")
            {
                datoConvertido = datoConvertir * .224809;
            }
        }
        
        if(unidadEntrada == "dina")
        {
            if(unidadSalida == "newtons")
            {
                datoConvertido = datoConvertir * 0.00001;
            }
            if(unidadSalida == "libra")
            {
                datoConvertido = datoConvertir * 0.0000022481;
            }
        }
        
        if(unidadEntrada == "libra")
        {
            if(unidadSalida == "dina")
            {
                datoConvertido = datoConvertir * 444822.16;
            }
            if(unidadSalida == "newtons")
            {
                datoConvertido = datoConvertir * 4.4482216;
            }
        }
    }
    
    
    
    
/*
    
    
    //funcion para la conversion de las unidades
    function forceConvert(initalValue, finalValue, convertionType){
        //convertions tyep
        switch (convertionType) {
            //newton a dina
            case 1:
                finalValue = initalValue * 100000;
                break;
            //dina a newton
            case 2:
                finalValue = initalValue / 100000;
            
            default:
                finalValue = 0;
        }
    }
*/
    const response = {
        statusCode: 200,
        DatoInicial: JSON.stringify(datoConvertir),
        unidadInicial: unidadEntrada,
        DatoConvertido: JSON.stringify(datoConvertido),
        Unidad: unidadSalida
    };
    return response;
};